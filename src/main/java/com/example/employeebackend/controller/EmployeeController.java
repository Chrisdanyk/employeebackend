package com.example.employeebackend.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.employeebackend.entity.*;
import com.example.employeebackend.exception.*;
import com.example.employeebackend.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/")
    public String index() {
        return "Welcome";
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) {
        Employee employee = employeeService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        return ResponseEntity.ok(employee);
    }

    @PostMapping("/employees")
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity editEmployee(@RequestBody Employee employee, @PathVariable("id") Long id) {
        Employee foundEmployee = employeeService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        foundEmployee.setFirstName(employee.getFirstName());
        foundEmployee.setLastName(employee.getLastName());
        Employee updatedEmployee = employeeService.save(foundEmployee);
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/employees/{id}")
    public ResponseEntity <Map<String,Boolean>> deleteEmployee(@PathVariable("id") Long id) {
        Employee foundEmployee = employeeService.findById(id).orElseThrow(() -> new ResourceNotFoundException("employee with this id not found!!!"));
        employeeService.delete(foundEmployee);
        Map<String,Boolean> response= new HashMap<>();
        response.put("Deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);

    }
}
