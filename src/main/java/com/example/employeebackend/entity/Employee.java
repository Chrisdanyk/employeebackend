package com.example.employeebackend.entity;

import javax.persistence.*;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 50, name = "\"firstName\"")
    private String firstName;
    @Column(length = 50, name = "\"lastName\"")
    private String lastName;
    @Column(length = 50, name = "\"emailId\"")
    private String emailId;

    public Employee() {
        
    }

    public Employee(String firstName,String lastName,String emailId) {
        this.firstName=firstName;
        this.firstName=lastName;
        this.emailId=emailId;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
